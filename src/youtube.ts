/* This file is part of PeerTubeify.
 *
 * PeerTubeify is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * PeerTubeify is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * PeerTubeify. If not, see <https://www.gnu.org/licenses/>.
 */

import * as _ from 'lodash/fp';
import * as browser from 'webextension-polyfill';

import { htmlToElement, getPeertubeVideoURL } from './util';
import { MessageKind, RedirectType } from './types';
import Preferences from './preferences'

const thumbnailURL = (host, path) => `https://${host}${path}`;
const isYouTube = _.contains('youtube.com', document.location.hostname);
const LINK_ID = 'peertube-link';

function searchVideo(query) {
    return browser.runtime.sendMessage({
        kind: MessageKind.SearchByName,
        query,
    });
}

async function peertubeify(query: String) {
    const prefs = await Preferences.getPreferences();

    switch (prefs.redirectYoutube) {
        case RedirectType.Show: {
            searchVideo(query)
                .then(async video => {
                    const url = getPeertubeVideoURL(video, prefs)
                    const link = videoLink(url, video);

                    removeVideoLink();

                    const querySelector = isYouTube ? 'ytd-app' : 'body';
                    document.querySelector(querySelector).appendChild(link);
                }).catch(removeVideoLink);
            break;
        }
        case RedirectType.None: {
            break;
        }
    }
}

if (isYouTube) {
    const throttledPeertubeify = _.throttle(1000, peertubeify);
    const observer = new MutationObserver(function(mutationsList) {
        for (const mutation of mutationsList) {
            if ((mutation.target as Element).classList.contains('ytp-title-link')) {
                for (const node of mutation.addedNodes) {
                    if (node.nodeType == Node.TEXT_NODE) {
                        throttledPeertubeify(node.textContent);
                    }
                }
            }
        }
    });

    observer.observe(document.body, {
        childList: true,
        subtree: true,
    })
} else {
    peertubeify(document.title.substring(0, document.title.indexOf(' - Invidious')));
}

const backgroundColor = isYouTube ? 'var(--yt-swatch-primary)' : '#fff';
const buttonColor = isYouTube ? 'var(--yt-swatch-text)' : '#000';
const textColor = isYouTube ? 'var(--yt-primary-text-color)' : '#000';
const hostSize = isYouTube ? '1.4rem' : '0.9rem';

const videoLink = (url, video) => htmlToElement(`
<div id="${LINK_ID}"
  style="
    position: fixed;
    bottom: 0;
    left: 0;
    box-sizing: border-box;
    width: 100%;
    padding: 1rem;
    z-index: 101;
    box-shadow: inset 0px 4px 8px -3px rgba(17, 17, 17, .06);
    background-color: ${backgroundColor};
    letter-spacing: normal;
  ">

  <button
    onclick="document.getElementById('${LINK_ID}').remove()"
    style="
      all: unset;
      color: ${buttonColor};
      cursor: pointer;
      font-size: 24px;
      position: absolute;
      line-height: .8;
      top: 1rem;
      right: 1rem;">
    ⨯
  </button>

  <a
    style="
      display: flex;
      align-items: center;
      text-decoration: none;"
    href=${url}>
    <img
      style="height: 110px;"
      src="${thumbnailURL(video.account.host, video.thumbnailPath)}">
    <div style="
        font-size: 18px;
        margin-left: 1rem;
        color: ${textColor};
        display: flex;
        flex-direction: column;
        justify-content: space-around;
      ">
      <p style="margin: 0 0 1rem;">
        ${video.name}
      </p>

      <p style="font-size: ${hostSize}; margin: 0;">
        ${video.account.host}
      </p>
    </div>
  </a>
</div>
`);

function removeVideoLink() {
    const existingLink = document.getElementById(LINK_ID);
    existingLink && existingLink.remove();
}
