/* This file is part of PeerTubeify.
 *
 * PeerTubeify is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * PeerTubeify is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * PeerTubeify. If not, see <https://www.gnu.org/licenses/>.
 */

import * as _ from 'lodash/fp';
import * as browser from 'webextension-polyfill';

import { htmlToElement, getPeertubeVideoURL } from './util';
import { MessageKind, RedirectType } from './types';
import { ResumeManager } from './resume';
import Preferences from './preferences';

const thumbnailURL = (host, path) => `https://${host}${path}`;

const LINK_ID = 'peertube-link';

function searchVideo() {
    const id = _.last(_.split('/', location.href));

    return browser.runtime.sendMessage({
        kind: MessageKind.SearchByID,
        id
    });
}

async function peertubeify() {
    const prefs = await Preferences.getPreferences();

    const isPreferredInstance = _.equals(prefs.searchInstance, location.hostname);

    if (isPreferredInstance) {
        return;
    }

    switch (prefs.redirectPeertube) {
        case RedirectType.Show: {
            searchVideo()
                .then(async video => {
                    const link = videoLink(getPeertubeVideoURL(video, prefs), video);
                    removeVideoLink();
                    document.querySelector('body').appendChild(link);
                }).catch(removeVideoLink);
            break;
        }
        case RedirectType.None: {
            break;
        }
    }
}

const videoLink = (url, video) => htmlToElement(`
<div id="${LINK_ID}"
  style="
    position: fixed;
    bottom: 0;
    left: 0;
    box-sizing: border-box;
    width: 100%;
    padding: 1rem;
    z-index: 10000;
    box-shadow: inset 0px 4px 8px -3px rgba(17, 17, 17, .06);
    background-color: #000;
  ">

  <button
    onclick="document.getElementById('${LINK_ID}').remove()"
    style="
      all: unset;
      color: #fff;
      cursor: pointer;
      font-size: 24px;
      position: absolute;
      line-height: .8;
      top: 1rem;
      right: 1rem;">
    ⨯
  </button>

  <a
    style="
      display: flex;
      align-items: center;
      text-decoration: none;"
    href=${url}>
    <img
      style="height: 110px;"
      src="${thumbnailURL(video.account.host, video.thumbnailPath)}">
    <div style="
        font-size: 18px;
        margin-left: 1rem;
        color: #fff;
        display: flex;
        flex-direction: column;
        justify-content: space-around;
      ">
      <p style="margin: 0 0 1rem;">
        ${video.name}
      </p>
    </div>
  </a>
</div>
`);

function removeVideoLink() {
    const existingLink = document.getElementById(LINK_ID);
    existingLink && existingLink.remove();
}

(async function () {
    const resumeManager = await ResumeManager.create();

    const throttledPeertubeify = _.throttle(1000, peertubeify);
    const observer = new MutationObserver(function(mutationsList) {
        for (const mutation of mutationsList) {
            console.log('peertubeify ' + (mutation.target as Element).id)
            if ((mutation.target as Element).id == 'video-element-wrapper') {
                throttledPeertubeify();
                resumeManager.resume()
            }
        }
    });
    resumeManager.start();

    observer.observe(document.body, {
        childList: true,
        subtree: true,
    });
})();
